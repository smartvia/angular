import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddressComponent} from "./address/address.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {TableComponent} from "./table/table.component";
import {DragDropComponent} from "./drag-drop/drag-drop.component";
import {TreeComponent} from "./tree/tree.component";
import {SearchComponent} from "./search/search.component";

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'address', component: AddressComponent },
  { path: 'table', component: TableComponent },
  { path: 'drag-drop', component: DragDropComponent },
  { path: 'tree', component: TreeComponent },
  { path: 'search', component: SearchComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
