import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formular',
  templateUrl: './formular.component.html',
  styleUrls: ['./formular.component.css']
})
export class FormularComponent implements OnInit {

  public name = "Peter"
  public names = ["Peter", "Juraj", "Ivan", "Barbora"]

  constructor() { }

  ngOnInit(): void {
  }

}
