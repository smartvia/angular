export class University {
  constructor(
    public name: string,
    public country: string,
    public web: string
  ) {
  }
}
