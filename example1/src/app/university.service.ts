import { Injectable } from '@angular/core';
import {University} from "./model/University";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UniversityService {

  // public universities = [
  //   new University("TUKE", "Slovakia", "http://tuke.sk"),
  //   new University("STUBA", "Slovakia", "http://stuba.sk"),
  //   new University("EUBA", "Slovakia", "http://euba.sk"),
  //   new University("UPJS", "Slovakia", "http://upjs.sk"),
  // ]

  private apiUrl = "http://universities.hipolabs.com/search?country=";

  constructor(private http: HttpClient) { }

  getUniversities(country = "Slovakia") {
    return this.http.get<University[]>(this.apiUrl + country);
    // return this.universities;
  }
}
