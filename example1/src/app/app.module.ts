import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormularComponent } from './formular/formular.component';
import {FormsModule} from "@angular/forms";
import { UniversitiesComponent } from './universities/universities.component';
import {UniversityService} from "./university.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    FormularComponent,
    UniversitiesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [UniversityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
