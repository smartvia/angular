import { Component, OnInit } from '@angular/core';
import {University} from "../model/University";
import {UniversityService} from "../university.service";

@Component({
  selector: 'app-universities',
  templateUrl: './universities.component.html',
  styleUrls: ['./universities.component.css']
})
export class UniversitiesComponent implements OnInit {

  // public universities = ["TUKE", "STUBA", "EUBA", "UPJS"]
  public universities: University[];

  public selectedUniversity: University;

  public country: string;

  constructor(private universityService: UniversityService) { }

  testSomething(){
    return false
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    // this.universities =
    this.universityService.getUniversities(this.country).subscribe(
      (universitiesFromService) => {
        this.universities = universitiesFromService
      }
    )
  }

  selectUniversity(university: University) {
    console.log("CLICK")
    console.log(university)
    this.selectedUniversity = university
  }

}
